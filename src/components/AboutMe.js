import React, {Component} from 'react';
import PropTypes from 'prop-types';

import styles from'../styles/AboutMe.module.css';
import mountains from '../assets/mountains.jpg';

function AboutMe ({name="Your name here", food="Your food here", number='your number'}) {

    return(
        <div className={styles.box}>
            <img className={styles.box__image} src={mountains}/>
            <h4>Name: {name}</h4>
            <h5>Food: {food}</h5>
            <h5>Number: {number}</h5>
        </div>
    )
}

AboutMe.propTypes = {
    name: PropTypes.string,
    food: PropTypes.string,
    number: PropTypes.number,
}

export default AboutMe;