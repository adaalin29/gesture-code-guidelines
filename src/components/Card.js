import React, {Component} from 'react';
import PropTypes from 'prop-types';

import styles from'../styles/Card.module.css';
import AboutMe from'../components/AboutMe.js';
import QuoteBox from'../components/QuoteBox';

function Card ( {ronism, getQuote} ) {

    return(
        
        <div className={styles.card}>
            <AboutMe name="Adamita Alin" food="Pizza" number={4}/>
            <QuoteBox ronism = {ronism} getQuote = {getQuote}/>
        </div>
    )
}

QuoteBox.PropTypes = {
    ronism: PropTypes.string,
    getQuote: PropTypes.func.isRequired,
}
export default Card;

