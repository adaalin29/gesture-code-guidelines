import React, {Component} from 'react';
import axios from 'axios';

import styles from'../styles/MainContainer.module.css';
import Card from '../components/Card.js';

export class MainComponent extends Component{
    // DATA: WILL STORE STRING FROM API CALL
    state = {data:''};

    //  CALLS API FOR A QUOTE AND CALLS updateState()
    getQuote = async() => {
        await axios('https://ron-swanson-quotes.herokuapp.com/v2/quotes')
            .then((res) => {
                const quote = res.data[0]
                this.updateQuote(quote);
            })
            .catch((error) => {
                console.error('Error fetching data', error)
            })
            .finally(() => {
                console.log('getData function ran')
            })
    }

    // FUNCTION UPDATES STATE OF DATA
    updateQuote = (quote) => {
        this.setState({ data: quote}, () => {
            // UPDATE CALLBACK
            console.log(this.state.data);
        });
    }

    componentDidMount(){
       this.getQuote();
    }

    render(){
        return(
            <div className={styles.main}>
                 <Card ronism = {this.state.data} getQuote = {this.getQuote}/>
            </div>
        )
    }
}

export default MainComponent;