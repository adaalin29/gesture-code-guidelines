import MainComponent from './pages/MainContainer.js';

function App() {
  return (
    <div>
      <MainComponent></MainComponent>
    </div>
  );
}

export default App;
